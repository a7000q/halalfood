<form method="POST" class="form" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Фото</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                @if(isset($product) && $product->url_photo)
                    <img src="{{$product->getUrlPhoto()}}" style="max-height: 200px;">
                @endif
                <input type="file" class="form-control time" name="photo"/>
                @error('photo')
                    <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Наименование</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                <input type="text" class="form-control" name="name" value="{{old('name') ?? $product->name ?? ""}}"/>
                @error('name')
                    <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Описание</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                <textarea class="form-control time" name="description" value="{{old('description') ?? $product->description ?? ""}}">
                </textarea>
                @error('description')
                    <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Базовая цена, руб</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                <input type="text" class="form-control time" name="price" value="{{old('price') ?? $product->price ?? ""}}"/>
                @error('price')
                    <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-lg-9 ml-lg-auto">
                <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                <a href="{{url('products')}}" class="btn btn-secondary">Отмена</a>
            </div>
        </div>
    </div>
</form>
