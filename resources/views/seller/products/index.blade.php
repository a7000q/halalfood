@extends('layout.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Торговая продукция</b></div>

                    <div class="card-body">

                        <div class="form-group">
                            <a href="{{url('/products/add')}}" class="btn btn-success btn-shadow"> <i class="flaticon2-plus-1"></i> Добавить</a>
                        </div>

                        <table class="table table-bordered table-hover" id="kt_datatable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Фото</th>
                                    <th>Наименование</th>
                                    <th>Описание</th>
                                    <th>Базовая цена, руб</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($products))
                                    @foreach($products as $k => $product)
                                        <tr>
                                            <td>{{$k + 1}}</td>
                                            <td>
                                                <img src="{{$product->getUrlPhoto()}}" style="max-height: 100px"/>
                                            </td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->description}}</td>
                                            <td>{{$product->price}}</td>
                                            <td>
                                                <a href="{{url("products/{$product->id}")}}">
                                                    <i class="flaticon-eye"></i>
                                                </a>
                                                <a href="{{url("products/edit/{$product->id}")}}">
                                                    <i class="flaticon-edit"></i>
                                                </a>
                                                <a href="{{url("products/remove/{$product->id}")}}" onclick="return confirm('Вы уверены?')">
                                                    <i class="flaticon-delete"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">
                                            <b>Торговая продукция еще не добавлена!</b>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
