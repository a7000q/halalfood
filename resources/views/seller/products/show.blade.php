@extends('layout.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Расширенный просмотр</b></div>

                    <div class="card-body">
                        <div class="form-group">
                            <a href="{{url('products')}}" class="btn btn-warning">
                                <i class="flaticon2-back"></i> Назад к списку
                            </a>
                            <a href="{{url("products/edit/".$product->id)}}" class="btn btn-success">
                                <i class="flaticon2-edit"></i> Редактировать
                            </a>
                            <a href="{{url("products/remove/".$product->id)}}" class="btn btn-danger" onclick="return confirm('Вы уверены?')">
                                <i class="flaticon-delete"></i> Удалить
                            </a>
                        </div>
                        <div class="form-group">
                            <table class="table">
                                <tr>
                                    <td><b>Фото</b></td>
                                    <td><img src="{{$product->getUrlPhoto()}}" style="max-height: 100px;"/></td>
                                </tr>
                                <tr>
                                    <td><b>Наименование</b></td>
                                    <td>{{$product->name}}</td>
                                </tr>
                                <tr>
                                    <td><b>Описание</b></td>
                                    <td>{{$product->description}}</td>
                                </tr>
                                <tr>
                                    <td><b>Базовая цена, руб</b></td>
                                    <td>{{$product->price}}</td>
                                </tr>
                            </table>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h3>Адреса</h3>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Название</th>
                                        <th>Адрес</th>
                                        <th>Доступен</th>
                                        <th>Цена</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($addresses as $k => $product_address)
                                        <tr>
                                            <td>{{$k + 1}}</td>
                                            <td>{{$product_address->sellerAddress->name}}</td>
                                            <td>{{$product_address->sellerAddress->address}}</td>
                                            <td>
                                                <div class="form-group row">
                                                    <div class="col-3">
                                                        <span class="switch switch-outline switch-icon switch-success">
                                                            <label>
                                                                <input type="checkbox"
                                                                       class="address-product"
                                                                       {{$product_address->enable?"checked":""}}
                                                                       data-id="{{$product_address->seller_address_id}}"/>
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text"
                                                           placeholder="{{$product->price}}"
                                                           value="{{$product_address->price}}"
                                                           class="form-control"
                                                           data-id="price_{{$product_address->id}}">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-success save-price" data-id="{{$product_address->id}}">Сохранить</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<form>
    @csrf
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function (){
        $(".address-product").change(function () {
            var id = $(this).attr('data-id');
            var product_id = {{$product->id}};
            var _token = $("input[name='_token']").val();
            console.log(id);
            $.ajax({
                url: "{{route('changeEnableProduct', ['product' => $product->id])}}",
                data: {product_id: product_id, address_id: id, _token:_token},
                type: 'POST',
                success: function (){
                    toastr.success('Сохранено');
                }
            });
        });

        $('.save-price').click(function (){
            var id = $(this).attr('data-id');
            var _token = $("input[name='_token']").val();
            let price_inp = 'price_' + id;
            let price = $("input[data-id='"+price_inp+"']").val();
            let data = {
                price: price
            };

            let url = "{{url('product-address')}}" + "/" + id;

            $.ajax({
                url: url,
                data: {data: data, _token:_token},
                type: 'POST',
                success: function (){
                    toastr.success('Сохранено');
                }
            });
        });
    });
</script>
