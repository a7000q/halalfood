@extends('layout.default')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>Новая продукция</b></div>
                @include('seller.products._form')
            </div>
        </div>
    </div>
</div>
@endsection
