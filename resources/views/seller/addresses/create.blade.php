@extends('layout.default')

@section('title', "Новая точка продаж")

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>Новая точка продаж</b></div>
                    @include('seller.addresses._form')
                </div>
            </div>
        </div>
    </div>
@endsection
