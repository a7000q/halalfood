@extends('layout.default')

@section('title', "$address->address")

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>{{$address->name}}</b></div>

                    <div class="card-body">
                        <div class="form-group">
                            <a href="{{url('/')}}" class="btn btn-warning">
                                <i class="flaticon2-back"></i> Назад к списку
                            </a>
                            <a href="{{url("addresses/edit/".$address->id)}}" class="btn btn-success">
                                <i class="flaticon2-edit"></i> Редактировать
                            </a>
                            <a href="{{url("addresses/remove/".$address->id)}}" class="btn btn-danger" onclick="return confirm('Вы уверены?')">
                                <i class="flaticon-delete"></i> Удалить
                            </a>
                        </div>
                        <div class="form-group">
                            <table class="table">
                                <tr>
                                    <td><b>Наименование</b></td>
                                    <td>{{$address->name}}</td>
                                </tr>
                                <tr>
                                    <td><b>Фото</b></td>
                                    <td><img src="{{$address->getUrlPhoto()}}" style="max-height: 100px;"/></td>
                                </tr>
                                <tr>
                                    <td><b>Аддрес</b></td>
                                    <td>{{$address->address}}</td>
                                </tr>
                                <tr>
                                    <td><b>Тип</b></td>
                                    <td>{{$address->typeText()}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
