@extends('layout.default')

@section('title', "Изменение адреса")

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header"><b>Изменение адреса</b></div>
                        @include('seller.addresses._form')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
