@extends('layout.default')

@section('title', 'Торговые точки продаж')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b>Торговые точки продаж</b></div>

                <div class="card-body">
                    <div class="form-group">
                        <a href="{{url('/addresses/add')}}" class="btn btn-success btn-shadow"> <i class="flaticon2-plus-1"></i> Добавить</a>
                    </div>

                    <table class="table table-bordered table-hover" id="kt_datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>Фото</th>
                            <th>Адрес</th>
                            <th>Тип</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($addresses) > 0)
                            @foreach($addresses as $k => $address)
                                <tr>
                                    <td>{{$k + 1}}</td>
                                    <td>{{$address->name}}</td>
                                    <td>
                                        <img src="{{$address->getUrlPhoto()}}" style="max-height: 100px"/>
                                    </td>
                                    <td>{{$address->address}}</td>
                                    <td>{{$address->typeText()}}</td>
                                    <td>
                                        <a href="{{url("addresses/{$address->id}")}}">
                                            <i class="flaticon-eye"></i>
                                        </a>
                                        <a href="{{url("addresses/edit/{$address->id}")}}">
                                            <i class="flaticon-edit"></i>
                                        </a>
                                        <a href="{{url("addresses/remove/{$address->id}")}}" onclick="return confirm('Вы уверены?')">
                                            <i class="flaticon-delete"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6"><b>Добавленных адресов не обнаруженно!</b> </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
