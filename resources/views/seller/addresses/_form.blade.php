<form method="POST" class="form" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Наименование</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                <input type="text" class="form-control time" name="name" value="{{old('name') ?? $address->name ?? ""}}"/>
                @error('name')
                    <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Адрес</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                <input type="text" class="form-control time" name="address" value="{{old('address') ?? $address->address ?? ""}}"/>
                @error('address')
                    <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Тип</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                <select class="form-control time" name="type">
                    @foreach(\App\Models\Seller\SellerAddress::$types as $id => $type)
                        <option value="{{$id}}" {{(isset($address) && $address->type == $id)?"selected":""}}>{{$type}}</option>
                    @endforeach
                </select>
                @error('type')
                <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Фото</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                @if(isset($address) && $address->url_photo)
                    <img src="{{$address->getUrlPhoto()}}" style="max-height: 200px;">
                @endif
                <input type="file" class="form-control time" name="photo"/>
                @error('photo')
                    <div style="color: red;">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-lg-9 ml-lg-auto">
                <button type="submit" class="btn btn-success mr-2">Сохранить</button>
                <a href="{{url('/')}}" class="btn btn-secondary">Отмена</a>
            </div>
        </div>
    </div>
</form>
