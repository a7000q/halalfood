@extends('layout.payment')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <a href="javascript:self.close();">Close</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
