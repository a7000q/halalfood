<?php
// Aside menu
return [

    'items' => [
        [
            'title' => 'Точки',
            'root' => true,
            'icon' => 'flaticon-pin', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/',
            'new-tab' => false,
        ],
        [
            'title' => 'Продукция',
            'root' => true,
            'icon' => 'flaticon2-cube', // or can be 'flaticon-home' or any flaticon-*
            'page' => 'products',
            'new-tab' => false,
        ]
    ]

];
