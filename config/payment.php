<?php

return [
    'user' => env('SBERBANK_USER', ""),
    'password' => env('SBERBANK_PASSWORD', "")
];
