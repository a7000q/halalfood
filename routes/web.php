<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [\App\Http\Controllers\Seller\UserController::class, 'login'])->name('login');
Route::post('auth', [\App\Http\Controllers\Seller\UserController::class, 'auth'])->name('auth');

Route::get('payment/success', [\App\Http\Controllers\Payment\PaymentController::class, 'success']);

Route::middleware(['auth'])->group(function () {
    Route::get('/', [\App\Http\Controllers\Seller\AddressesController::class, "index"]);
    Route::get('logout', [\App\Http\Controllers\Seller\UserController::class, 'logout'])->name('logout');

    Route::get('addresses/add', [\App\Http\Controllers\Seller\AddressesController::class, "create"]);
    Route::post('addresses/add', [\App\Http\Controllers\Seller\AddressesController::class, "store"]);
    Route::get('addresses/{address}', [\App\Http\Controllers\Seller\AddressesController::class, "show"]);
    Route::get('addresses/edit/{address}', [\App\Http\Controllers\Seller\AddressesController::class, "edit"]);
    Route::post('addresses/edit/{address}', [\App\Http\Controllers\Seller\AddressesController::class, "store"]);
    Route::get('addresses/remove/{address}', [\App\Http\Controllers\Seller\AddressesController::class, "delete"]);

    Route::get('products', [\App\Http\Controllers\Seller\ProductsController::class, "index"]);
    Route::get('products/add', [\App\Http\Controllers\Seller\ProductsController::class, "create"]);
    Route::post('products/add', [\App\Http\Controllers\Seller\ProductsController::class, "store"]);
    Route::get('products/{product}', [\App\Http\Controllers\Seller\ProductsController::class, "show"]);
    Route::get('products/edit/{product}', [\App\Http\Controllers\Seller\ProductsController::class, "edit"]);
    Route::post('products/edit/{product}', [\App\Http\Controllers\Seller\ProductsController::class, "store"]);
    Route::get('products/remove/{product}', [\App\Http\Controllers\Seller\ProductsController::class, "delete"]);

    Route::post('products/change-enable', [\App\Http\Controllers\Seller\ProductsController::class, "changeEnableAddress"])
        ->name('changeEnableProduct');

    Route::post('product-address/{productAddress}', [\App\Http\Controllers\Seller\ProductAddressController::class, "update"]);
});
