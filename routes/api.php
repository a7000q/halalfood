<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function (){
    Route::get('/me', function (){return Auth::user();});
    Route::post('/me', [\App\Http\Controllers\Customer\CustomerController::class, "update"]);
    Route::get("/pay", [\App\Http\Controllers\Customer\OrderController::class, "pay"]);
});

Route::post('/phone/code', [\App\Http\Controllers\Customer\CustomerController::class, "addCode"]);
Route::post('/phone/code/validate', [\App\Http\Controllers\Customer\CustomerController::class, "validateCode"]);
Route::post('/device/address', [\App\Http\Controllers\Customer\CustomerController::class, "setAddressForDevice"]);
Route::get('/addresses', [\App\Http\Controllers\Customer\CustomerController::class, "getSellerAddresses"]);
Route::post('/my-address', [\App\Http\Controllers\Customer\CustomerController::class, "getMyAddressForDevice"]);
Route::put('/my-address', [\App\Http\Controllers\Customer\CustomerController::class, "updateMyAddressForDevice"]);
Route::put('/basket/set', [\App\Http\Controllers\Customer\OrderController::class, "setProduct"]);
Route::post('/basket', [\App\Http\Controllers\Customer\OrderController::class, "getOrder"]);
