<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Services\Customer\OrderService;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function success(Request $request, OrderService $orderService){
        $request->validate([
            'orderId' => 'required'
        ]);

        $orderService->paySuccess($request->get('orderId'));

        return view('payment.success');
    }
}
