<?php

namespace App\Http\Controllers\Seller;

use App\Classes\Seller\SellerApp;
use App\Http\Controllers\Controller;
use App\Http\Requests\Seller\ChangeEnableProductRequest;
use App\Http\Requests\Seller\ProductRequest;
use App\Models\Seller\Product;
use App\Models\Seller\SellerAddress;
use App\Repositories\ProductRepository;


class ProductsController extends Controller
{
    public function index(){
        return view('seller.products.index', [
            'products' => \Auth::user()->seller->products
        ]);
    }

    public function create(){
        return view('seller.products.create');
    }

    public function store(ProductRequest $request, ProductRepository $repository, Product $product = null){
        $path = "";
        if ($request->exists('photo'))
            $path = $request->file('photo')->store('products');

        $array = $request->all(['name', 'description', 'price']);
        if ($path)
            $array['url_photo'] = $path;

        if (!$product)
            $product = $repository->add($array);
        else {
            SellerApp::checkAccess($product);
            $product->update($array);
        }

        return redirect('products/'.$product->id);
    }

    public function show(Product $product, ProductRepository $repository){
        SellerApp::checkAccess($product);
        return view('seller.products.show', [
            'product' => $product,
            'addresses' => $repository->getAddresses($product)
        ]);
    }

    public function edit(Product $product){
        SellerApp::checkAccess($product);
        return view('seller.products.edit', ['product' => $product]);
    }

    public function delete(Product $product, ProductRepository $repository){
        $repository->delete($product);
        return redirect('products');
    }

    public function changeEnableAddress(ProductRepository $repository, ChangeEnableProductRequest $request){
        $address = SellerAddress::findOrFail($request->get('address_id'));
        $product = Product::findOrFail($request->get('product_id'));
        SellerApp::checkAccess($product);
        SellerApp::checkAccess($address);
        return $repository->changeEnableAddress($product, $address);
    }
}
