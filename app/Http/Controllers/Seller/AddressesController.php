<?php

namespace App\Http\Controllers\Seller;

use App\Classes\Seller\SellerApp;
use App\Http\Controllers\Controller;
use App\Http\Requests\Seller\AddressRequest;
use App\Models\Seller\SellerAddress;
use App\Repositories\SellerAddressRepository;
use App\Services\SellerService;
use Illuminate\Http\Request;

class AddressesController extends Controller
{
    public function index(){
        return view('seller.addresses.index', [
            'addresses' => \Auth::user()->seller->addresses
        ]);
    }

    public function create(){
        return view('seller.addresses.create');
    }

    public function store(AddressRequest $request, SellerAddressRepository $repository, SellerAddress $address = null){
        $path = "";
        if ($request->exists('photo'))
            $path = $request->file('photo')->store('addresses');

        $array = $request->all(['address', 'type', 'name']);

        if ($path)
            $array['url_photo'] = $path;

        if (!$address)
            $address = $repository->add($array);
        else {
            SellerApp::checkAccess($address->seller);
            $address->update($array);
        }

        return redirect('addresses/'.$address->id);
    }

    public function show(SellerAddress $address){
        SellerApp::checkAccess($address->seller);
        return view('seller.addresses.show', ['address' => $address]);
    }

    public function edit(SellerAddress $address){
        SellerApp::checkAccess($address->seller);
        return view('seller.addresses.edit', ['address' => $address]);
    }

    public function delete(SellerAddress $address, SellerAddressRepository $repository){
        SellerApp::checkAccess($address->seller);
        $repository->delete($address);
        return redirect('/');
    }
}
