<?php

namespace App\Http\Controllers\Seller;

use App\Classes\Seller\SellerApp;
use App\Http\Controllers\Controller;
use App\Http\Requests\Seller\ProductAddressRequest;
use App\Models\Seller\ProductAddress;
use Illuminate\Http\Request;

class ProductAddressController extends Controller
{
    public function update(ProductAddress $productAddress, ProductAddressRequest $request){
        SellerApp::checkAccess($productAddress->sellerAddress);
        SellerApp::checkAccess($productAddress->product);
        $productAddress->update($request->get('data'));
        return $productAddress;
    }
}
