<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seller\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(){
        return \View::make('login');
    }

    public function auth(LoginRequest $request){
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/');
        }

        return back()->withErrors([
            'error_h1' => "Не удаётся войти.",
            'error_text' => 'Пожалуйста, проверьте правильность написания логина и пароля.'
        ]);
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
