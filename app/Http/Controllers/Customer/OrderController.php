<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\SetProductRequest;
use App\Services\Customer\CustomerService;
use App\Services\Customer\OrderService;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function setProduct(CustomerService $customerService, OrderService $service, SetProductRequest $request){
        $customerAddress = $customerService->getMyAddressForDevice($request->get('uid'));
        return $service->setProduct(
            $customerAddress,
            $request->getSellerAddress(),
            $request->getProduct(),
            $request->get('count')
        );
    }

    public function getOrder(Request $request, OrderService $service, CustomerService $customerService){
        $v = $request->validate([
            'uid' => 'required'
        ]);
        $customerAddress = $customerService->getMyAddressForDevice($request->get('uid'));
        return $service->getCurrentOrder($customerAddress);
    }

    public function pay(OrderService $orderService){
        $customer = \Auth::user();
        $customerAddress = $customer->address;
        $order = $orderService->getCurrentOrder($customerAddress);
        return $orderService->pay($order);
    }
}
