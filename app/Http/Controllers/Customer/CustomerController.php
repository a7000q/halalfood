<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\CustomerRequest;
use App\Http\Requests\Customer\CustomerUpdateRequest;
use App\Http\Requests\Customer\SetAddressRequest;
use App\Http\Requests\Customer\ValidCodeRequest;
use App\Services\Customer\CustomerService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function addCode(CustomerRequest $request, CustomerService $service){
        $service->putNewCodeForPhone($request->get('phone'), $request->get('uid'));
        return ['result' => 'ok'];
    }

    public function validateCode(ValidCodeRequest $request, CustomerService $service){
        $customer = $service->validateCodeByPhone($request->get('code'), $request->get('phone'));
        if (!$customer)
            abort(404, 'not customer');

        return $customer;
    }

    public function update(CustomerUpdateRequest $request){
        $customer = \Auth::user();
        $customer->update($request->all());
        return $customer;
    }

    public function setAddressForDevice(SetAddressRequest $request, CustomerService $service){
        $d = $request->all(['address', 'uid', 'lat', 'lon']);
        return $service->setAddressForDevice($d['uid'], $d['address'], $d['lat'], $d['lon']);
    }

    public function getSellerAddresses(CustomerService $service){
        return $service->getAddresses();
    }

    public function getMyAddressForDevice(Request $request, CustomerService $service){
        $validated = $request->validate([
            'uid' => 'required'
        ]);

        return $service->getMyAddressForDevice($request->get('uid'));
    }

    public function updateMyAddressForDevice(Request $request, CustomerService $service){
        $validated = $request->validate([
            'uid' => 'required',
            'data' => 'required|array'
        ]);

        $address = $service->getMyAddressForDevice($request->get('uid'));
        if (!$address)
            return [];

        return $service->updateAddress($address, $request->get('data'));
    }
}
