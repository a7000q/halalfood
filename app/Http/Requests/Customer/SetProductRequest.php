<?php

namespace App\Http\Requests\Customer;

use App\Models\Seller\Product;
use App\Models\Seller\SellerAddress;
use Illuminate\Foundation\Http\FormRequest;

class SetProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uid' => 'required|string',
            'product_id' => 'required|integer|exists:products,id',
            'count' => 'required|integer',
            'seller_address_id' => 'required|integer|exists:seller_addresses,id'
        ];
    }

    public function getProduct(){
        return Product::where(['id' => $this->get('product_id')])->first();
    }

    public function getSellerAddress(){
        return SellerAddress::where(['id' => $this->get('seller_address_id')])->first();
    }
}
