<?php

namespace App\Repositories\Customer;

use App\Models\Customer\Customer;
use App\Models\Customer\CustomerDevice;
use libphonenumber\PhoneNumberFormat;
use Propaganistas\LaravelPhone\PhoneNumber;

class CustomerRepository
{
    public function getByPhone($phone){
        $phone = $this->getValidPhone($phone);
        $customer = Customer::where(['phone' => $phone])->first();
        if (!$customer){
            $customer = new Customer();
            $customer->phone = $phone;
            $customer->save();
        }
        return $customer;
    }

    public function getByDeviceUid($uid){
        $customerDevice = CustomerDevice::where(['uid' => $uid])->first();
        if (!$customerDevice){
            $customer = Customer::create();

            $customerDevice = CustomerDevice::create([
                'customer_id' => $customer->id,
                'uid' => $uid
            ]);
        }

        return $customerDevice->customer;
    }

    public function getValidPhone($phone){
        $phone = (string) PhoneNumber::make($phone)
            ->ofCountry('RU')
            ->format(PhoneNumberFormat::INTERNATIONAL);

        $phone = (string) PhoneNumber::make($phone)
            ->formatE164();

        return $phone;
    }
}
