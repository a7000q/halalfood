<?php


namespace App\Repositories;


use App\Models\Seller\Product;
use App\Models\Seller\ProductAddress;
use App\Models\Seller\Seller;
use App\Models\Seller\SellerAddress;

class ProductRepository
{
    protected $seller;

    public function __construct(){
        $this->seller = \Auth::user()->seller;
    }

    public function add(array $array){
        $array['seller_id'] = $this->seller->id;
        $product = Product::firstOrCreate($array);
        return $product;
    }

    public function delete(Product $product){
        if ($product->url_photo)
            \Storage::delete($product->url_photo);

        $product->delete();
    }

    public function enableForAddress(Product $product, SellerAddress $address){
        $product_address = $this->findProductAddress($product, $address);
        $product_address->enable = true;
        $product_address->save();

        return $product_address;
    }

    public function disableForAddress(Product $product, SellerAddress $address){
        $product_address = $this->findProductAddress($product, $address);
        $product_address->enable = false;
        $product_address->save();

        return $product_address;
    }

    public function findProductAddress(Product $product, SellerAddress $address){
        $product_address = ProductAddress::firstOrCreate([
            'product_id' => $product->id,
            'seller_address_id' => $address->id
        ]);

        $product_address->refresh();

        return $product_address;
    }

    public function getAddresses(Product $product){
        $addresses = $this->seller->addresses;

        $result = [];
        foreach ($addresses as $address)
            $result[] = $this->findProductAddress($product, $address);

        return $result;
    }

    public function changeEnableAddress(Product $product, SellerAddress $address){
        $product_address = $this->findProductAddress($product, $address);
        if ($product_address->enable)
            $this->disableForAddress($product, $address);
        else
            $this->enableForAddress($product, $address);

        return $product_address;
    }
}
