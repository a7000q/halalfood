<?php


namespace App\Repositories;

use App\Models\Seller\Product;
use App\Models\Seller\Seller;
use App\Models\Seller\SellerAddress;

class SellerAddressRepository
{
    protected $seller;
    protected $productRepository;

    public function __construct(ProductRepository $productRepository){
        $this->seller = \Auth::user()->seller;
        $this->productRepository = $productRepository;
    }

    public function add(array $array){
        $adr = SellerAddress::firstOrCreate([
            'address' => $array["address"],
            'seller_id' => $this->seller->id,
            'name' => $array["name"]
        ]);

        if (isset($array['url_photo']))
            $adr->url_photo = $array['url_photo'];

        if (isset($array['type']))
            $adr->type = $array['type'];

        $adr->save();

        $products = Product::where(['seller_id' => $this->seller->id])->get();
        foreach ($products as $product)
            $this->productRepository->findProductAddress($product, $adr);

        return $adr;
    }

    public function delete(SellerAddress $address){
        foreach ($address->productAddresses as $productAddress)
            $productAddress->delete();

        $address->delete();
    }
}
