<?php


namespace App\Services\Customer;

use App\Models\Customer\CustomerAddress;
use App\Models\Order\Order;
use App\Models\Order\OrderItem;
use App\Models\Order\OrderSberbankPay;
use App\Models\Seller\Product;
use App\Models\Seller\SellerAddress;
use xnf4o\Sberbank;


class OrderService
{
    protected $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function setProduct(CustomerAddress $customerAddress, SellerAddress $sellerAddress, Product $product, $count){
        $order = $this->getCurrentOrder($customerAddress);

        if (!$order){
            $order = $this->addOrder($sellerAddress, $customerAddress);
        }

        if ($order->seller_address_id != $sellerAddress->id){
            foreach ($order->items as $item){
                $item->delete();
            }

            $order->update([
                'seller_id' => $sellerAddress->seller_id,
                'seller_address_id' => $sellerAddress->id
            ]);
        }

        $item = $order->getItemByProduct($product);
        if (!$item){
            $item = $this->addItem($order, $product, $count);
        }

        if ($count == 0) {
            $item->delete();
        }
        else{
            $item->update(['count' => $count]);
        }

        $order->refresh();
        $order->calcTotalSum();

        $order->items->load('product');

        return $order;
    }

    public function getCurrentOrder(CustomerAddress $customerAddress){
        $order = Order::where([
            'customer_address_id' => $customerAddress->id,
            'status' => Order::STATUS_NEW
        ])->with(['items.product', 'sberbankPay'])->first();

        if (!$order)
            return [];

        return $order;
    }

    public function addOrder(SellerAddress $sellerAddress, CustomerAddress $customerAddress){
        $order = Order::create([
            'seller_address_id' => $sellerAddress->id,
            'customer_address_id' => $customerAddress->id,
            'seller_id' => $sellerAddress->seller_id,
            'customer_id' => $customerAddress->customer_id,
            'door_code' => $customerAddress->door_code,
            'floor' => $customerAddress->floor,
            'entrance' => $customerAddress->entrance
        ]);

        return $order;
    }

    public function addItem(Order $order, Product $product, $count){
        $price = $product->getPriceBySellerAddress($order->sellerAddress);
        $item = new OrderItem([
            'product_id' => $product->id,
            'count' => $count,
            'price' => $price
        ]);

        $order->items()->save($item);

        return $item;
    }

    public function pay(Order $order){
        $user = \Config::get('payment.user');
        $password = \Config::get('payment.password');
        $sberbank = new Sberbank(false, [
            'login' => $user,
            'password' => $password
        ]);

        $token = \Hash::make($order->id.$user.$password.time());

        $payment = [
            'orderNumber'   => $order->id." - ".$order->customer_id." - ".$order->seller_id,
            'amount'        => $order->total_sum,
            'language'      => 'ru',
            'description'   => '',
            'currency'      => '',
            'returnUrl'     => url('/payment/success'),
            'failUrl'       => url('/payment/error'),
        ];

        if (!$order->sberbankPay) {
            $orderSberbank = new OrderSberbankPay([
                'token' => $token
            ]);

            $order->sberbankPay()->save($orderSberbank);
        }
        else {
            $order->load('sberbankPay');
            if ($order->sberbankPay->form_url)
                return $order;
        }

        $paymentResponse = $sberbank->paymentURL($payment);

        if ($paymentResponse["success"]){
            $data = json_decode($paymentResponse["response"]);
            $order->sberbankPay()->update([
                'sberbank_order_id' => $data->orderId,
                'payment_url' => $data->formUrl
            ]);
        }

        $order->setStatus(Order::STATUS_PAYING);

        $order->load('sberbankPay');

        return $order;
    }

    public function paySuccess($orderId){
        $sberbank_order = OrderSberbankPay::where(['sberbank_order_id' => $orderId])->first();

        if ($sberbank_order){
            /** @var Order $order */
            $order = $sberbank_order->order;
            $order->setStatus(Order::STATUS_PAID);
            $order->paid = 1;
            $order->save();
        }
    }
}
