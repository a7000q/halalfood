<?php

namespace App\Services\Customer;


use App\Models\Customer\Customer;
use App\Models\Customer\CustomerAddress;
use App\Models\Customer\CustomerDevice;
use App\Models\Customer\CustomerSmsCode;
use App\Models\Seller\SellerAddress;
use App\Repositories\Customer\CustomerRepository;

class CustomerService
{
    protected $repository;

    public function __construct(CustomerRepository $repository){
        $this->repository = $repository;
    }

    public function putNewCodeForPhone($phone, $uid){
        $phone = $this->repository->getValidPhone($phone);
        $customer = $this->repository->getByDeviceUid($uid);
        $customer_code = CustomerSmsCode::where([
            'customer_id' => $customer->id
        ])->first();

        if (!$customer_code){
            $customer_code = new CustomerSmsCode();
            $customer_code->customer_id = $customer->id;
            $customer_code->save();
        }

        $customer_code->code = (string)mt_rand(1111, 9999);
        $customer_code->phone = $phone;
        $customer_code->save();
        $customer->update([
            'status' => Customer::STATUS_CODE_SEND
        ]);
    }

    public function validateCodeByPhone($code, $phone){
        $phone = $this->repository->getValidPhone($phone);
        $smsCode = CustomerSmsCode::where(['code' => $code, 'phone' => $phone])->first();

        if (!$smsCode)
            return false;

        $customer = $smsCode->customer;

        $customer->token = \Hash::make($customer->phone.time().$code);
        $customer->status = Customer::STATUS_AUTHORIZED;
        $customer->phone = $smsCode->phone;
        $customer->save();

        $smsCode->delete();

        return $customer;
    }

    public function setAddressForDevice($uid, $address, $lat, $lon){
        $device = CustomerDevice::where(['uid' => $uid])->first();

        if (!$device){
            $customer = Customer::create();

            $device = CustomerDevice::create([
                'customer_id' => $customer->id,
                'uid' => $uid
            ]);
        }

        $customer = $device->customer;
        $customerAddress = CustomerAddress::where([
            'customer_id' => $customer->id,
        ])->first();

        if (!$customerAddress){
            $customerAddress = CustomerAddress::create([
                'address' => $address,
                'lat' => $lat,
                'lon' => $lon,
                'customer_id' => $customer->id
            ]);
        }else{
            $customerAddress->update([
                'address' => $address,
                'lat' => $lat,
                'lon' => $lon
            ]);
        }

        return $customerAddress;
    }

    public function getAddresses(){
        $addresses = SellerAddress::get(['id', 'address', 'url_photo', 'type', 'name'])
            ->makeHidden(['address', 'typeText', 'url_photo']);

        return $addresses;
    }

    public function getMyAddressForDevice($uid){

        $device = CustomerDevice::where(['uid' => $uid])->first();

        if (!$device)
            return [];

        $customer = $device->customer;

        if (!$customer->address)
            return [];

        return $customer->address;
    }

    public function updateAddress(CustomerAddress $address, array $data){
        $address->update($data);
        return $address;
    }
}
