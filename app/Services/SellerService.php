<?php

namespace App\Services;

use App\Models\Seller\Product;
use App\Models\Seller\Seller;
use App\Models\Seller\SellerAddress;
use App\Models\User;

class SellerService
{
    public function add($name, $email, $password){
        $seller = Seller::firstOrCreate([
            'name' => $name
        ]);

        $user = User::firstOrCreate([
            'seller_id' => $seller->id,
            'email' => $email,
            'password' => \Hash::make($password),
            'name' => $email
        ]);

        return $seller;
    }
}
