<?php

namespace App\Models\Seller;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Seller\Product
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $seller_id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $url_photo
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUrlPhoto($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Seller\Seller|null $seller
 * @property-read mixed $photo
 * @property-read mixed $price_text
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'seller_id',
        'name',
        'description',
        'price',
        'url_photo'
    ];

    protected $appends = ['photo', 'priceText', 'price'];

    public function getUrlPhoto(){
        return ($this->url_photo)?asset("images/".$this->url_photo):asset('media/images/pngegg.png');
    }

    public function seller(){
        return $this->hasOne(Seller::class, "id", "seller_id");
    }

    public function getPhotoAttribute(){
        return asset("images/".$this->url_photo);
    }

    public function getPriceTextAttribute(){
        return number_format($this->price)." ₽";
    }

    public function getPriceAttribute(){

        if (isset($this->pivot) && $this->pivot->price){
            return $this->pivot->price;
        }

        return $this->attributes['price'];
    }

    public function getPriceBySellerAddress(SellerAddress $sellerAddress){
        $address_product = ProductAddress::where([
            'seller_address_id' => $sellerAddress->id,
            'product_id' => $this->id,
            'enable' => true
        ])->first();

        if ($address_product && $address_product->price)
            return $address_product->price;

        return $this->price;
    }
}
