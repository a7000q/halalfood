<?php

namespace App\Models\Seller;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Seller\SellerAddress
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $seller_id
 * @property string $address
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Seller\Seller|null $seller
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Seller\ProductAddress[] $productAddresses
 * @property-read int|null $product_addresses_count
 * @property int $type
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereType($value)
 * @property string|null $url_photo
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereUrlPhoto($value)
 * @property string $name
 * @property-read mixed $type_text
 * @method static \Illuminate\Database\Eloquent\Builder|SellerAddress whereName($value)
 * @property-read mixed $address_without_city
 * @property-read mixed $enable_products
 * @property-read mixed $photo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Seller\Product[] $products
 * @property-read int|null $products_count
 */
class SellerAddress extends Model
{
    use HasFactory;

    const TYPE_SHOP = 1;
    const TYPE_CAFE = 2;

    static public $types = [
        self::TYPE_SHOP => 'Магазин',
        self::TYPE_CAFE => 'Кафе'
    ];

    protected $fillable = [
        'address',
        'seller_id',
        'type',
        'url_photo',
        'name'
    ];

    protected $appends = ['typeText', 'addressWithoutCity', 'photo', 'enableProducts'];

    public function typeText(){
        return static::$types[$this->type];
    }

    public function seller(){
        return $this->hasOne(Seller::class, "id", "seller_id");
    }

    public function productAddresses(){
        return $this->hasMany(ProductAddress::class, "seller_address_id", "id");
    }

    public function getUrlPhoto(){
        return ($this->url_photo)?asset("images/".$this->url_photo):asset('media/images/pngegg.png');
    }

    public function getTypeTextAttribute(){
        return $this->typeText();
    }

    public function getAddressWithoutCityAttribute(){
        $result = explode(",", $this->address);
        unset($result[0]);
        $address = trim(implode(",", $result));
        $address = str_replace("ул.", "", $address);
        $address = trim($address);
        return $address;
    }

    public function getPhotoAttribute(){
        return asset("images/".$this->url_photo);
    }

    public function products(){
        return $this->belongsToMany(Product::class, "product_addresses");
    }

    public function getEnableProductsAttribute(){
        return $this->products()->where(['enable' => 1])->withPivot(['price'])->get();
    }
}
