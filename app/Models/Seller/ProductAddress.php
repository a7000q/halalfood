<?php

namespace App\Models\Seller;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Seller\ProductAddress
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $product_id
 * @property int $seller_address_id
 * @property int $enable
 * @property string $price
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress whereEnable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress whereSellerAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAddress whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Seller\Product|null $product
 * @property-read \App\Models\Seller\SellerAddress|null $sellerAddress
 */
class ProductAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'seller_address_id',
        'enable',
        'price'
    ];

    public function sellerAddress(){
        return $this->hasOne(SellerAddress::class, "id", "seller_address_id");
    }

    public function product(){
        return $this->hasOne(Product::class, "id", "product_id");
    }
}
