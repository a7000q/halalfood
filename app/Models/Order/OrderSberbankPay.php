<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order\OrderSberbankPay
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $order_id
 * @property string $status
 * @property string $sberbank_order_id
 * @property string $payment_url
 * @property string $token
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay wherePaymentUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay whereSberbankOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSberbankPay whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderSberbankPay extends Model
{
    protected $fillable = [
        'token',
        'sberbank_order_id',
        'payment_url',
        'status'
    ];

    public function order(){
        return $this->hasOne(Order::class, "id", "order_id");
    }
}
