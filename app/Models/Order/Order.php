<?php

namespace App\Models\Order;

use App\Models\Seller\Product;
use App\Models\Seller\SellerAddress;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order\Order
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property int $seller_id
 * @property int $seller_address_id
 * @property int $customer_address_id
 * @property int|null $door_code
 * @property int|null $floor
 * @property int|null $entrance
 * @property string $total_sum
 * @property int $paid
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDoorCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereEntrance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSellerAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTotalSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order\OrderItem[] $items
 * @property-read int|null $items_count
 */
class Order extends Model
{
    use HasFactory;

    const STATUS_NEW = 1;
    const STATUS_PAYING = 2;
    const STATUS_PAID = 3;
    const STATUS_PAID_ERROR = 4;
    const STATUS_CLOSE = 10;

    protected $fillable = [
        'seller_address_id',
        'customer_address_id',
        'customer_id',
        'seller_id',
        'door_code',
        'entrance',
        'floor'
    ];

    protected $appends = ['total_count'];

    public function getItemByProduct(Product $product){
        $item = $this->items()->where(['product_id' => $product->id])->first();
        return $item;
    }

    public function items(){
        return $this->hasMany(OrderItem::class, "order_id", "id");
    }

    public function sellerAddress(){
        return $this->hasOne(SellerAddress::class, "id", "seller_address_id");
    }

    public function calcTotalSum(){
        $sum = $this->items->sum('sum');
        $this->total_sum = $sum;
        $this->save();
    }

    public function getTotalCountAttribute(){
        return $this->items()->sum('count');
    }

    public function sberbankPay(){
        return $this->hasOne(OrderSberbankPay::class, "order_id", "id");
    }

    public function setStatus($status){
        $this->status = $status;
        $this->save();
    }
}
