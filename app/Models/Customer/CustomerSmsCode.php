<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Customer\CustomerSmsCode
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property string $code
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $phone
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerSmsCode wherePhone($value)
 * @property-read \App\Models\Customer\Customer|null $customer
 */
class CustomerSmsCode extends Model
{
    use HasFactory;

    protected $fillable = [
        'code'
    ];

    public function customer(){
        return $this->hasOne(Customer::class, "id", "customer_id");
    }
}
