<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Customer\CustomerAddress
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property string $address
 * @property string $lat
 * @property string $lon
 * @property string|null $office
 * @property string|null $door_code
 * @property string|null $floor
 * @property string|null $entrance
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereDoorCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereEntrance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereLon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerAddress whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $address_without_city
 * @property-read mixed $city
 */
class CustomerAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'address', 'lat', 'lon',
        'floor',
        'entrance',
        'door_code'
    ];

    protected $appends = ['addressWithoutCity', 'city'];

    public function getAddressWithoutCityAttribute(){
        $array = explode(",", $this->address);
        unset($array[0]);
        $address = implode(",", $array);
        $address = str_replace("улица", "", $address);
        $address = str_replace("ул.", "", $address);
        $address = trim($address);

        return $address;
    }

    public function getCityAttribute(){
        $array = explode(",", $this->address);
        return $array[0];
    }
}
