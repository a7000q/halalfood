<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Customer\Customer
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $phone
 * @property string|null $token
 * @property string|null $name
 * @property string|null $surname
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Customer\CustomerSmsCode|null $smsCode
 * @property-read \App\Models\Customer\CustomerAddress|null $address
 */
class Customer extends Model
{
    use HasFactory;

    const STATUS_UNAUTHORIZED = 1;
    const STATUS_AUTHORIZED = 10;
    const STATUS_CODE_SEND = 2;

    protected $fillable = [
        'name',
        'surname'
    ];

    protected $hidden = [
        'smsCode'
    ];



    public function smsCode(){
        return $this->hasOne(CustomerSmsCode::class, "customer_id", "id");
    }

    public function address(){
        return $this->hasOne(CustomerAddress::class, "customer_id", "id");
    }
}
