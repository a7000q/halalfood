<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Customer\CustomerDevice
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property string $uid
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerDevice whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Customer\Customer|null $customer
 */
class CustomerDevice extends Model
{
    use HasFactory;

    protected $fillable = ['uid', 'customer_id'];

    public function customer(){
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}
