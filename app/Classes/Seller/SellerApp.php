<?php

namespace App\Classes\Seller;

use App\Models\Seller\Seller;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerApp
{
    static public function checkAccess(Model $object){
        if ($object instanceof Seller)
            if ($object->id != \Auth::user()->seller->id)
                throw new HttpException(404);
            else
                return true;

        if (!isset($object->seller_id))
            throw new \Exception('not seller_id');

        if (\Auth::user()->seller->id != $object->seller_id)
            throw new HttpException(404);

        return true;
    }
}
