<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('seller_id');
            $table->unsignedBigInteger('seller_address_id');
            $table->unsignedBigInteger('customer_address_id');
            $table->integer('door_code')->nullable();
            $table->integer('floor')->nullable();
            $table->integer('entrance')->nullable();
            $table->decimal('total_sum')->default(0);
            $table->boolean('paid')->default(false);
            $table->integer('status')->default(App\Models\Order\Order::STATUS_NEW);

            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('seller_id')->references('id')->on('sellers');
            $table->foreign('seller_address_id')->references('id')->on('seller_addresses');
            $table->foreign('customer_address_id')->references('id')->on('customer_addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
